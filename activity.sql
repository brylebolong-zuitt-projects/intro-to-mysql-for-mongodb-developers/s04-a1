A. 

SELECT name FROM artists WHERE name LIKE "%D%";

B. 

SELECT title, length
FROM songs
WHERE length > 230;

C.

SELECT * FROM albums JOIN songs ON 
albums.id = songs.album_id;


D.

SELECT * FROM artists 
WHERE name LIKE “%A%”,
AND JOIN albums ON artists.id = albums.artist_id;


E.

SELECT * FROM albums
WHERE name <= 4  
ORDER BY name DESC;



F.

SELECT * FROM albums JOIN songs ON songs.id = albums.album_id,
SELECT name FROM albums ORDER BY title DESC,
SELECT title FROM songs ORDER BY title ASC;

